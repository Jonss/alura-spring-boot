package br.com.jonss.listavip.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by joao.santana on 25/06/17.
 */

@Controller
public class ConvidadoController {

    @RequestMapping("/")
    public String index() {
        return "index";
    }
}
