FROM openjdk:8-jdk

RUN mkdir /listaVip
COPY target/listaVip-1.0-SNAPSHOT.jar /listaVip/listaVip-1.0-SNAPSHOT.jar
WORKDIR /listaVip

CMD ["sh", "-c", "java -Dspring.profiles.active=$JAVA_ENV -jar listaVip-1.0-SNAPSHOT.jar"]